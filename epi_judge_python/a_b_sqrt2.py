from typing import List

from test_framework import generic_test


def generate_first_k_a_b_sqrt2(k):
    if k <= 1:
        return []

    primes = [2]

    start = 2
    while start < k**0.5:

    return primes


if __name__ == '__main__':
    exit(
        generic_test.generic_test_main('a_b_sqrt2.py', 'a_b_sqrt2.tsv',
                                       generate_first_k_a_b_sqrt2))
